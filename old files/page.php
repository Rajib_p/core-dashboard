<?php 
require 'app/start.php';

if (empty($_GET['page'])) {
	$page = false;
	} else {
		//Grab the page
		/*var_dump($_GET);*/
		$slug = $_GET['page'];
			// we will not use query here as we did in index.php as this is user controlled, an sql injection my possible. Here we prepare and execute.
		$page = $db->prepare("
			SELECT *
			FROM pages
			WHERE slug = :slug
			LIMIT 1
			");
			
// this right hand slug is a place holder, we will execute ir with $slug variable from line 9

		$page->execute(['slug' => $slug]);
		//time to pull the data
		$page = $page->fetch(PDO::FETCH_ASSOC);

		/*var_dump($page);*/ //ok

		if($page) {
			$page['created'] = new DateTime($page['created']);
			if ($page['updated']) {
				$page['updated'] = new DateTime($page['updated']);
			}
		}

	}

require VIEW_ROOT . '/page/show.php';




?>