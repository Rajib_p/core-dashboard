<?php require VIEW_ROOT . '/templates/header.php'; ?>

	<?php if (!$page):  ?>
			<p>No page found Sorry</p>
	<?php else: ?>

		<h2><?php echo $page['title']; ?></h2>

		<?php echo $page['body']; ?>

		<p class="faded">Created on <?php echo $page['created']->format('jS M Y h:i a'); ?>
			<?php if ($page['updated']): ?>
				Last updated on <?php echo $page['updated']->format('jS M Y h:i a'); ?>
			<?php endif; ?>
		</p>
	<?php endif; ?>



<!-- <script>alert(1)</script> -->



<?php require VIEW_ROOT . '/templates/footer.php'; ?>