<?php require VIEW_ROOT . '/templates/header.php'; ?>


	<h3>
		<a href="<?php echo BASE_URL; ?>/admin/index.php">
			<span></span>Back
		</a>
	</h3>

<?php if(empty($texts)): ?>
		<p>No text data in the table at the moment.</p>
<?php else: ?>
<table>
	<thead>
		<tr>
			<th>Label</th>
			<th>Title</th>
			<th>Heading</th>
			<th>body</th>
			<th></th>
			
		</tr>
	</thead>

	<tbody>
		<?php foreach($texts as $txt): ?>
		<tr>
			<td><?php echo $txt['label']; ?></td>
			<td><?php echo $txt['title']; ?></td>
			<td><?php echo $txt['heading']; ?></td>
			<td><?php echo $txt['body']; ?></td>
			<td><a href="<?php echo BASE_URL; ?>/admin/edit.php?id=<?php echo $txt['id']; ?>">Edit</a></td>
			<td>

			<!-- <a href=" -->

			<?php //echo BASE_URL; ?>

			<!-- /admin/delete.php?id= -->

			<?php //echo $txt['id']; ?>
			<!-- ">Delete</a></td> -->


		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<?php endif; ?>
<a href="<?php echo BASE_URL; ?>/admin/add.php">Add a new page</a>


<?php require VIEW_ROOT . '/templates/footer.php'; ?>