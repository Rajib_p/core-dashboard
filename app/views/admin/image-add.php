<?php
require '../app/start.php';
/*require 'db-connect.php';*/
$upload_dir = 'upload/';

if(isset($_POST['btnsave'])){
	/*echo 'Ok';*/
	$name = $_POST['name'];
	$position =$_POST['position'];

	$imgName = $_FILES['myfile']['name'];
	$imgTmp = $_FILES['myfile']['tmp_name'];
	$imgSize = $_FILES['myfile']['size'];

	if(empty($name)){
		$errMgs = 'Please input a name for the image';
		}
	elseif (empty($position)){
		$errMgs = 'Please input a position for the image';
		}
	elseif(empty($imgName)){
		$errMgs = 'Please select an image';
		
		}
	else{
			
		$imgExt = strtolower(pathinfo($imgName, PATHINFO_EXTENSION));
			
		$allowExt = array('jpeg', 'jpg', 'png', 'gif');
			
		$userPic = time().'_'.rand(1000, 9999) .'.'.$imgExt;
			
			if(in_array($imgExt, $allowExt)){
				
				if($imgSize < 10000000){
					move_uploaded_file($imgTmp, $upload_dir.$userPic);
	          		print_r(error_get_last());
				}else{
					$errMgs = 'Image is too large';
				}
			}else{
				$errMgs = 'Please select a valid image';
			}
		}

			
		/*	if(!isset($errMgs)){
				$sql = "insert into tbl_photo(name, position, photo)
						values('" .$name. "', '" .$position. "', '" .$userPic. "')";
				$result = mysqli_query($conn, $sql);
				if($result){
					$successMsg = 'New record added successfully';
					header('refresh:3;index.php');
				} else {
					$errMgs = 'Error:'.mysqli_error($db);
				}

			}*/


  if(!isset($errMgs))
  {

	   $stmt = $db->prepare("

	   			INSERT INTO images(name, 
	   								  position, 
	   								  photo) 
	   							VALUES(:name, 
	   									:position, 
	   									:photo)
	   						");

	   
/*	   $stmt->bindParam(':name',$name);
	   $stmt->bindParam(':position',$position);
	   $stmt->bindParam(':photo',$userPic);

	   print_r(error_get_last());
	   print_r($stmt->errorInfo());*/

		
	   $stmt->execute([

	   			'name' => $name,
				'position' => $position,
				'photo' => $userPic,
	   	]);
		/*
		print_r(error_get_last());
		print_r($stmt->errorInfo());*/


	   if($stmt->execute())
	   {
	    $successMgs = "new record succesfully inserted ...";
	    header("refresh:3;image-index.php"); // redirects image view page after 5 seconds.
	   }
	   else
	   {
	    $errMgs = "error while inserting....";
	   }


	}

}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>working with image</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h3>image upload and edit</h3> 

	<h3>
		<a href="image-index.php">
			<span></span>Back
		</a>
	</h3>


<?php 
	
	if (isset($errMgs)) {
	echo $errMgs;
	}

 ?>

<?php 
	
	if (isset($successMgs)) {
	echo $successMgs.'redirecting ... ... ... ...';
	}

 ?>

	<form action="" method="post" enctype="multipart/form-data">

	<label for="name">Name</label>
	<input type="text" name="name">

	<label for="position">Position</label>
	<input type="text" name="position">

	<label for="photo">Photo</label>
	<input type="file" name="myfile">

	<label>Submit</label>
	<button type="submit" name="btnsave">Save</button>

	</form>
</body>
</html>