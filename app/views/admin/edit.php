<?php require VIEW_ROOT . '/templates/header.php'; ?>

<h2>Edit page</h2>
<form action="<?php echo BASE_URL; ?>/admin/edit.php" method='POST' autocomplete="off">
<label for="label">
	Label
	<input type="hidden" name="label" id="label" value="<?php echo $texts['label']; ?>">
</label>
	<label for="title">
		Title
		<input type="text" name="title" id="title" value="<?php echo $texts['title']; ?>">
	</label>

	<label for="heading">
		Heading
		<input type="text" name="heading" id="heading" value="<?php echo $texts['heading']; ?>">
	</label>

	<label for="body">
		Body
		<textarea name="body" id="body" cols="30" rows="10"><?php echo $texts['body']; ?></textarea>
	</label>

	<input type="hidden" name="id" value="<?php echo $texts['id']; ?>">

	<input type="submit" value="Edit">

</form>


<?php require VIEW_ROOT . '/templates/footer.php'; ?>