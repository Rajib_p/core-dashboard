<?php
require '../app/start.php';
$upload_dir = 'upload/';


if(isset($_GET['id'])){
	$id = $_GET['id'];

	/*echo $id;*/

	$row = $db->prepare("
	SELECT *
	FROM images
	WHERE id = :id
	");
	$row->execute(['id' => $_GET['id']]);
	$row = $row->fetch(PDO::FETCH_ASSOC);

}


if(isset($_POST['btnUpdate'])){
	/*echo 'Ok';*/
	$name = $_POST['name'];
	$position =$_POST['position'];

	$imgName = $_FILES['myfile']['name'];
	$imgTmp = $_FILES['myfile']['tmp_name'];
	$imgSize = $_FILES['myfile']['size'];

	if(empty($name)){
		$errMgs = 'Please input a name for the image';
	}elseif (empty($position)){
		$errMgs = 'Please input a position for the image';
	}else{
		//if no new photo is selected then keep the old image
		$userPic = $row['photo'];
	}


	if($imgName){
		//get image extension
		$imgExt = strtolower(pathinfo($imgName, PATHINFO_EXTENSION));
		
		$allowExt = array('jpeg', 'jpg', 'png', 'gif');
		
		$userPic = time().'_'.rand(1000, 9999) .'.'.$imgExt;
		
		if(in_array($imgExt, $allowExt)){
		
			if($imgSize < 5000000){
				unlink($upload_dir.$row['photo']);
				move_uploaded_file($imgTmp, $upload_dir.$userPic);
          		print_r(error_get_last());

			}else{
				$errMgs = 'Image is too large';
			}
		}else{
			$errMgs = 'Please select a valid image';
		}
	}




	//check uploaded file not error than insert data into database
	if(!isset($errMgs)){


//mysqli query :D 

/*		$sql = "update tbl_photo
								set name = '".$name."',
									position = '".$position."',
									photo = '".$userPic."'
			where id=".$id;
		print_r(error_get_last());
		$result = mysqli_query($conn, $sql);*/

//PDO query

try {
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "UPDATE images SET name = '".$name."',
									position = '".$position."',
									photo = '".$userPic."'
			where id=".$id;

    $stmt = $db->prepare($sql);
    $stmt->execute();
    /*echo $stmt->rowCount() . " records UPDATED successfully";*/
    $successMgs = $stmt->rowCount()." new record succesfully updated ...";
	header("refresh:10;image-index.php");
    }
catch(PDOException $e)
    {
    echo $sql . "<br>" . $e->getMessage();
    }



/*
	$updateTbl = $db->prepare("
		UPDATE tbl_photo
		SET 
			name = :name,
			position = :position,
			photo = :photo,
		WHERE id = :id
		");

	$updateTbl->execute([
		'id' => $id,
		'name' => $name,
		'position' => $position,
		'photo' => $userPic,
		]);
		print_r(error_get_last());
		print_r($updateTbl->errorInfo());
*/

	}



}

 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>working with image</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h3>image upload and edit</h3> 

	<h3>
		<a href="image-index.php">
			<span></span>Back
		</a>
	</h3>


<?php 
	
	if (isset($errMgs)) {
	echo $errMgs;
	}

 ?>

<?php 
	
	if (isset($successMgs)) {
	echo $successMgs.'redirecting ... ... ... ... ';
	}

 ?>

	<form action="" method="post" enctype="multipart/form-data">

	<label for="name">Name</label>
	<input type="text" name="name" value="<?php echo $row['name']; ?>">

	<label for="position">Position</label>
	<input type="text" name="position" value="<?php echo $row['position']; ?>">

	<label for="photo">Photo</label>
	<img src="<?php echo $upload_dir.$row['photo']; ?>" width="200">
	<input type="file" name="myfile" >

	<label>Submit</label>
	<button type="submit" name="btnUpdate">Update</button>

	</form>
</body>
</html>