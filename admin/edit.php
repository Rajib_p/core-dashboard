<?php 
require '../app/start.php';

/*require VIEW_ROOT . '/admin/edit.php';*/

if(!empty($_POST)){
	$id = $_POST['id'];
	$label = $_POST['label'];
	$title = $_POST['title'];
	$heading = $_POST['heading'];
	$body = $_POST['body'];

	$updatePage = $db->prepare("
		UPDATE texts
		SET 
			label = :label,
			title = :title,
			heading = :heading,
			body = :body,
			created = created,
			updated = NOW()
		WHERE id = :id
		");

	$updatePage->execute([
		'id' => $id,
		'label' => $label,
		'title' => $title,
		'body' => $body,
		'heading' => $heading,
		]);

	header('Location: ' . BASE_URL . '/admin/index.php');
}


if(!isset($_GET['id']) ){
	header('Location: ' . BASE_URL . '/admin/index.php');
	die();

}

$texts = $db->prepare("
	SELECT *
	FROM texts
	WHERE id = :id
	");
$texts->execute(['id' => $_GET['id']]);
$texts = $texts->fetch(PDO::FETCH_ASSOC);


require VIEW_ROOT . '/admin/edit.php';

 ?>