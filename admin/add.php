<?php 
require '../app/start.php';

require VIEW_ROOT . '/admin/add.php'; 

if(!empty($_POST)){
	$label = $_POST['label'];
	$title = $_POST['title'];
	$heading = $_POST['heading'];
	$body = $_POST['body'];

	$insertPage = $db->prepare("
			INSERT INTO texts (label, title, heading, body)
			VALUES (:label, :title, :heading, :body)
		");

	$insertPage->execute([
			'label' => $label,
			'title' => $title,
			'heading' => $heading,
			'body' => $body,
		]);

	header ('Location: ' .BASE_URL . '/admin/index.php');

}

 ?>