<?php 
require 'app/start.php';
$upload_dir = 'admin/upload/';
 
$texts = $db->query("
        SELECT *
        FROM texts

    ")->fetchAll(PDO::FETCH_ASSOC);
$rows = $db->query("
        SELECT *
        FROM images

    ")->fetchAll(PDO::FETCH_ASSOC);

/*var_dump($texts); //ok*/
require VIEW_ROOT . '/home.php';


 ?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Elegant</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="style.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">


        <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->




    </head>

    <body>
        

<div class="top-menu-bg">

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-header">
        <a class="navbar-brand" href="#"><img src="img/logo.png" alt=""></a>
        <ul class="navbar-toggle">
            <li>MENU</li>
            <li><button type="button" class="bttn" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button></li>
        </ul>

      </div>
      <div class="navbar-collapse collapse text-center">
        <ul class="nav navbar-nav nav-text-style">
          <li class="active"><b>WHO</b></li>
          <li class="each-nav"><a href="#about">About Us</a></li>
          <li class="each-nav"><a href="#contact">Blog</a></li>

          <li class="active"><b>WHAT</b></li>
          <li class="each-nav"><a href="#about">See Our Projects</a></li>
          <li class="each-nav"><a href="#contact">Project Page</a></li>

          <li class="active"><b>WHERE</b></li>
          <li class="each-nav"><a href="#about">Contact Us</a></li>

          <li class="active"><b>FOLLOW US</b></li>
          <li class="social facebook"><a href="#about">Facebook</a></li>
          <li class="social instagram"><a href="#contact">Blog</a></li>
          <li class="social debrill"><a href="#contact">Blog</a></li>
          <li class="social twitter"><a href="#contact">Blog</a></li>
        </ul>
      </div>
    </nav>
        <div class="top-text">
        <h2><?php echo $text1; ?></h2>
        </div>

</div>



<div class="who-we-are padding-before">
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="text-color header-area text-center">

        <h4><?php echo $text2; ?></h4>
        <h2><?php echo $text3; ?></h2>
        <p><?php echo $text4; ?></p>

        <div class="butn">
            <a href="">Read More About Us</a>
        </div>
              
        </div>
</div>
</div>
</div>


<div class="what-we-do padding-before">

        <div class="header-area text-center text-color">

        <h4>What We Do</h4>
        <h2>Show Your Amazing Work</h2>
        <div class="row">
            <div class="col-md-3">
                <a href=""><img src="<?php echo $weDo1; ?>" alt=""></a>
                <div class="caption">
                    hi
                </div>
            </div>
            <div class="col-md-3">
                <a href=""><img src="img/we-do-2.jpg" alt=""></a>
                <div class="caption">
                    hi
                </div>
            </div>
            <div class="col-md-3">
                <a href=""><img src="img/we-do-3.jpg" alt=""></a>
                <div class="caption">
                    hi
                </div>
            </div>
            <div class="col-md-3">
                <a href=""><img src="img/we-do-4.jpg" alt=""></a>
                <div class="caption">
                    hi
                </div>
            </div>
        </div>        
        <div class="row">
            <div class="col-md-3">
                <a href=""><img src="img/we-do-5.jpg" alt=""></a>
                <div class="caption">
                    hi
                </div>
            </div>
            <div class="col-md-3">
                <a href=""><img src="img/we-do-6.jpg" alt=""></a>
                <div class="caption">
                    hi
                </div>
            </div>
            <div class="col-md-3">
                <a href=""><img src="img/we-do-7.jpg" alt=""></a>
                <div class="caption">
                    hi
                </div>
            </div>
            <div class="col-md-3">
                <a href=""><img src="img/we-do-8.jpg" alt=""></a>
                <div class="caption">
                    hi
                </div>
            </div>
        </div>

              
        </div>

</div>



<div class="who-we-are padding-before">
<div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="text-color header-area text-center">

        <h4>Who we Are</h4>
        <h2>The Amazing People Behind This</h2>
        <p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
        <div class="managing-people">

        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="individual">
                <img src="img/tobias-van-schneider.png" alt="">
                <div class="designation text-left">
                    <h4 class="special-color">CEO</h4>
                    <h3>Tobias Schneider</h3>
                    <p>Lorem ipsum dolor sit amet, conse tetuer adi piscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="individual">
                <img src="img/jack-knife.png" alt="">
                <div class="designation text-left">
                    <h4 class="special-color">DESIGNER</h4>
                    <h3>Jack Knife</h3>
                    <p>Lorem ipsum dolor sit amet, conse tetuer adi piscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                </div>
            </div>
        </div>
            
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="individual">
                <img src="img/ricki-hall.png" alt="">
                <div class="designation text-left">
                    <h4 class="special-color">DEVELOPER</h4>
                    <h3>Ricki Hall</h3>
                    <p>Lorem ipsum dolor sit amet, conse tetuer adi piscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                </div>
            </div>
        </div>
        </div>
              
        </div>
</div>
</div>
</div>


<div class="last-post padding-before">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="text-color header-area text-center">

                <h4>LAST POST</h4>
                <h2>We Like to Write</h2>
            </div>

        <div class="col-md-6">
            <div class="write">
                <img src="img/write.jpg" alt="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="text">
                <div class="date">14 September 2014</div>
                <h3>Change Your Space</h3>
                <p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores.</p>
            </div>

                <div class="butn-read">
                    <a href="">Read</a>
                </div>
        </div>

                  
            </div>
        
    </div>

        <div class="more text-center butn-read">
            <a href="">More From Our Blog</a>
        </div>


</div>

<div class="padding-before">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="text-color header-area text-center">
                <h4>CONTACT US</h4>
                <h2>WOrk With Us</h2>
            </div>

<div class="col-md-12">
    <div class="write">
               <!--  <img src="img/write.jpg" alt=""> -->
    <div id="map" style="width:100%;height:500px"></div>

    <script>
      function initMap() {

        var uluru = {lat: 22.804892, lng: 89.542102};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 9,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
<!--     <script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdyZkWInk61ppgRY2B_ef0qrn0r1UoPkk&callback=initMap">
</script> -->

            </div>
        </div>
        </div>             
            </div>
        
    </div>




<div class="last-post">
<div class="row">
    <div class="col-md-8 col-md-offset-2 background-shadow padding-before">

        <div class="loction text-center">

            <div class="col-md-6">
                <div class="write">
                    <h4>LOCATION</h4>
                    <p>198 West 21th Street, New</br>
                    York, NY 10010</p>

                </div>

                <div class="fax">
                    <h4>FAX</h4>
                    <p>+88 (0) 202 0000 000</br>
                     +88 (0) 202 0000 000</p>

                </div>

            </div>
            <div class="col-md-6">
                <div class="text">
                    <h4>PHONE</h4>
                    <p>+88 (0) 101 0000000</br>
                    +88 (0) 101 0000000</p>
                    
                </div>

                    <div class="email">
                    <h4>EMAIL</h4>
                    <p>elegant@elegant.com
                    commercial@elegant.com</p>
                    
                </div>

            </div>

        </div>  
    </div>
        
</div>
</div>


<div class="last-post padding-before background-shadow">
<!-- <div class="background-shadow"> -->
<div class="row">
    <div class="col-md-8 col-md-offset-2">

        <div class="col-md-6">
            <div class="footer-left">© 2014 Designed and Developed by Diogo Dantas</div>

        </div>
        <div class="col-md-6">
            <div class="footer-right">Email: imdiogodantas@gmail.com</div>
        </div>

    </div>
<!-- </div> -->
</div>
</div>


        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
    </body>

</html>
